# Avoid multiple calls to find_package to append duplicated properties to the targets
include_guard()########### VARIABLES #######################################################################
#############################################################################################

set(mathpack_COMPILE_OPTIONS_RELEASE
    "$<$<COMPILE_LANGUAGE:CXX>:${mathpack_COMPILE_OPTIONS_CXX_RELEASE}>"
    "$<$<COMPILE_LANGUAGE:C>:${mathpack_COMPILE_OPTIONS_C_RELEASE}>")

set(mathpack_LINKER_FLAGS_RELEASE
    "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${mathpack_SHARED_LINK_FLAGS_RELEASE}>"
    "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${mathpack_SHARED_LINK_FLAGS_RELEASE}>"
    "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${mathpack_EXE_LINK_FLAGS_RELEASE}>")

set(mathpack_FRAMEWORKS_FOUND_RELEASE "") # Will be filled later
conan_find_apple_frameworks(mathpack_FRAMEWORKS_FOUND_RELEASE "${mathpack_FRAMEWORKS_RELEASE}" "${mathpack_FRAMEWORK_DIRS_RELEASE}")

set(mathpack_LIBRARIES_TARGETS "") # Will be filled later


######## Create an interface target to contain all the dependencies (frameworks, system and conan deps)
if(NOT TARGET mathpack_DEPS_TARGET)
    add_library(mathpack_DEPS_TARGET INTERFACE)
endif()

set_property(TARGET mathpack_DEPS_TARGET
             PROPERTY INTERFACE_LINK_LIBRARIES
             $<$<CONFIG:Release>:${mathpack_FRAMEWORKS_FOUND_RELEASE}>
             $<$<CONFIG:Release>:${mathpack_SYSTEM_LIBS_RELEASE}>
             $<$<CONFIG:Release>:>
             APPEND)

####### Find the libraries declared in cpp_info.libs, create an IMPORTED target for each one and link the
####### mathpack_DEPS_TARGET to all of them
conan_package_library_targets("${mathpack_LIBS_RELEASE}"    # libraries
                              "${mathpack_LIB_DIRS_RELEASE}" # package_libdir
                              mathpack_DEPS_TARGET
                              mathpack_LIBRARIES_TARGETS  # out_libraries_targets
                              "_RELEASE"
                              "mathpack")    # package_name

# FIXME: What is the result of this for multi-config? All configs adding themselves to path?
set(CMAKE_MODULE_PATH ${mathpack_BUILD_DIRS_RELEASE} ${CMAKE_MODULE_PATH})


########## GLOBAL TARGET PROPERTIES Release ########################################
    set_property(TARGET mathpack::mathpack
                 PROPERTY INTERFACE_LINK_LIBRARIES
                 $<$<CONFIG:Release>:${mathpack_OBJECTS_RELEASE}>
                 $<$<CONFIG:Release>:${mathpack_LIBRARIES_TARGETS}>
                 APPEND)

    if("${mathpack_LIBS_RELEASE}" STREQUAL "")
        # If the package is not declaring any "cpp_info.libs" the package deps, system libs,
        # frameworks etc are not linked to the imported targets and we need to do it to the
        # global target
        set_property(TARGET mathpack::mathpack
                     PROPERTY INTERFACE_LINK_LIBRARIES
                     mathpack_DEPS_TARGET
                     APPEND)
    endif()

    set_property(TARGET mathpack::mathpack
                 PROPERTY INTERFACE_LINK_OPTIONS
                 $<$<CONFIG:Release>:${mathpack_LINKER_FLAGS_RELEASE}> APPEND)
    set_property(TARGET mathpack::mathpack
                 PROPERTY INTERFACE_INCLUDE_DIRECTORIES
                 $<$<CONFIG:Release>:${mathpack_INCLUDE_DIRS_RELEASE}> APPEND)
    set_property(TARGET mathpack::mathpack
                 PROPERTY INTERFACE_COMPILE_DEFINITIONS
                 $<$<CONFIG:Release>:${mathpack_COMPILE_DEFINITIONS_RELEASE}> APPEND)
    set_property(TARGET mathpack::mathpack
                 PROPERTY INTERFACE_COMPILE_OPTIONS
                 $<$<CONFIG:Release>:${mathpack_COMPILE_OPTIONS_RELEASE}> APPEND)

########## For the modules (FindXXX)
set(mathpack_LIBRARIES_RELEASE mathpack::mathpack)
