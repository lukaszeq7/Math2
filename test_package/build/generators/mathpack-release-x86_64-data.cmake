########### AGGREGATED COMPONENTS AND DEPENDENCIES FOR THE MULTI CONFIG #####################
#############################################################################################

set(mathpack_COMPONENT_NAMES "")
set(mathpack_FIND_DEPENDENCY_NAMES "")

########### VARIABLES #######################################################################
#############################################################################################
set(mathpack_PACKAGE_FOLDER_RELEASE "C:/Users/lukasz/.conan/data/mathpack/0.1/_/_/package/5a61a86bb3e07ce4262c80e1510f9c05e9b6d48b")
set(mathpack_BUILD_MODULES_PATHS_RELEASE )


set(mathpack_INCLUDE_DIRS_RELEASE "${mathpack_PACKAGE_FOLDER_RELEASE}/include")
set(mathpack_RES_DIRS_RELEASE )
set(mathpack_DEFINITIONS_RELEASE )
set(mathpack_SHARED_LINK_FLAGS_RELEASE )
set(mathpack_EXE_LINK_FLAGS_RELEASE )
set(mathpack_OBJECTS_RELEASE )
set(mathpack_COMPILE_DEFINITIONS_RELEASE )
set(mathpack_COMPILE_OPTIONS_C_RELEASE )
set(mathpack_COMPILE_OPTIONS_CXX_RELEASE )
set(mathpack_LIB_DIRS_RELEASE "${mathpack_PACKAGE_FOLDER_RELEASE}/lib")
set(mathpack_LIBS_RELEASE mathpack)
set(mathpack_SYSTEM_LIBS_RELEASE )
set(mathpack_FRAMEWORK_DIRS_RELEASE )
set(mathpack_FRAMEWORKS_RELEASE )
set(mathpack_BUILD_DIRS_RELEASE "${mathpack_PACKAGE_FOLDER_RELEASE}/")


set(mathpack_COMPONENTS_RELEASE )