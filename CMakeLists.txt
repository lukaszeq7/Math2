cmake_minimum_required(VERSION 3.15)
project(mathpack CXX)

add_library(mathpack src/mathpack.cpp)
target_include_directories(mathpack PUBLIC include)

set_target_properties(mathpack PROPERTIES PUBLIC_HEADER "include/mathpack.h")
install(TARGETS mathpack)
