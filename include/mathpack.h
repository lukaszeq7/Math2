#pragma once

#ifdef _WIN32
  #define mathpack_EXPORT __declspec(dllexport)
#else
  #define mathpack_EXPORT
#endif

mathpack_EXPORT void mathpack();
